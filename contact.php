<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sky Key</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <!-- Styles -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" /><!-- Bootstrap -->
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" /><!-- Owl Carousal -->	
    <link rel="stylesheet" href="css/icons.css" type="text/css" /><!-- Font Awesome -->
    <link rel="stylesheet" href="css/select2.min.css" type="text/css" /><!-- Select2 -->
    <link rel="stylesheet" href="css/perfect-scrollbar.css" /><!-- Scroll Bar -->
    <link rel="stylesheet" href="css/style.css" type="text/css" /><!-- Style -->	
    <link rel="stylesheet" href="css/responsive.css" type="text/css" /><!-- Responsive -->		
    <link rel="stylesheet" href="css/colors/color.css" type="text/css" /><!-- Color -->

</head>
<body itemscope="">
    <div class="theme-layout">
        
        <?php
            require_once("header.php");
        ?>

        <div class="page-top blackish overlape">
            <div class="parallax" data-velocity="-.1" style="background: url(https://placehold.it/1600x700) repeat scroll 0 0"></div>
            <div class="container">
                <div class="page-title">
                    <span>ИДЕАЛЬНО ДЛЯ ВАС</span>
                    <h3>НАШИ КОНТАКТЫ</h3>
                </div><!-- Page Title -->
            </div>
        </div><!-- Page Top -->

        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 column">
                        <div class="map">
                            <div id="map-canvas"></div>
                        </div><!-- Google Map -->
                    </div>
                </div>
            </div>
        </section><!-- Google Map Section -->


        <section class="block remove-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="region-contact-info team-detail-info">
                                        <div class="heading2">
                                            <span>БЫСТРО И СОХРАННО</span>
                                            <h3>АДРЕСС ОФИСА</h3>
                                        </div>
                                        <p>Цитата</p>
                                        <div class="contact-detail">
                                            <span class="contact">
                                                <i class="fa fa-mobile"></i>
                                                <strong>No ТЕЛЕФОНА</strong>
                                                <span>+1917-658-8070</span>
                                                <span>+1917-658-8070</span>
                                            </span>
                                            <span class="contact">
                                                <i class="fa fa-envelope"></i>
                                                <strong>Email АДРЕСС</strong>
                                                <span>sk.uz@@yandex.ru</span>
                                            </span>
                                            <span class="contact">
                                                <i class="fa fa-clock-o"></i>
                                                <strong>Офисное время</strong>
                                                <span>10:00am - 06:00pm / Воскресение: Выходной</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="get-quote-form contact-info-form">
                                        <div class="heading2">
                                            <span>быстро и сохранно</span>
                                            <h3>Что либо</h3>
                                        </div>
                                        <p>цитата</p>
                                        <div class="contactform">
                                            <div id="formresult"></div>
                                            <form id="contactform" method="post" action="contact.php">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" placeholder="Введите Имя" class="text-field input-style" id="name" name="name" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="email" placeholder="Email Адресс" id="email" name="email" class="text-field input-style" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" placeholder="Subject" name="subject" class="text-field input-style" />
                                                    </div>
                                                    <div class="col-md-12">
                                                        <textarea placeholder="Логин" class="text-field input-style" id="Сообщение" name="desc"></textarea>
                                                    </div>
                                                    <div class="col-md-12"><div class="g-recaptcha" data-sitekey="6"></div></div>
                                                    <div class="col-md-12">
                                                        <button id="quote-btn" title="" itemprop="url" type="submit" class="theme-btn"><i class="fa fa-paper-plane"></i>Отправить</button>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>	


        <footer>
            <section class="block">
                <div class="parallax dark" data-velocity="-.2" style="background: rgba(0, 0, 0, 0) url(https://placehold.it/1600x700) no-repeat 50% 0;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 column">
                                    <div class="widget">
                                        <div class="about-widget">
                                            <div class="logo">
                                                <a itemprop="url" href="index.html" title=""><img itemprop="image" src="images/resource/logo.png" alt="" /></a>
                                            </div>
                                            <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Адрес доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Описание сервиса</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Таможенные лимиты</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Ограничения доставки</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Блог компании</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Расчет мощности LI-Ion АКБ</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            <ul class="social-btn">
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-facebook"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 column">
                                    <Div class="row">
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Условия доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Получение посылок</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Отслеживание посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Доставка негабарита</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Доставка с Амазона</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Ребейты</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>Услуги доставки</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="services-detail2.html" title="">ПЭ Стандарт</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail6.html" title="">Доставка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Отправка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail4.html" title="">Страхование посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Доставка с Ебей</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail.html" title="">Форум</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 column">
                                    <div class="widget blue1">
                                        <div class="heading2">
                                            <span>БЫСТРО И НАДЕЖНО</span>
                                            <h3>ОТПРАВИТЬ ПИСЬМО</h3>
                                        </div>
                                        <div class="subscription-form">
                                            <p itemprop="description">ЦИТАТА ИЛИ ЕЩЕ ЧТО ТО</p>
                                            <form>
                                                <input type="text" placeholder="Enter Your Email Address" />
                                                <a title="" href="#" class="theme-btn" data-toggle="modal" data-target="#submission-message"><i class="fa fa-paper-plane"></i>ОТПРАВИТЬ</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bottom-line">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 column">
                            <span>&copy; 2015 <a itemprop="url" title="" href="index.html">Unload</a> - All Rights Reserved - Made By Webinane</span>
                        </div>
                        <div class="col-md-6 column">
                            <ul>
                                <li><a itemprop="url" href="index.html" title="">HOME</a></li>
                                <li><a itemprop="url" href="services.html" title="">SERVICES</a></li>
                                <li><a itemprop="url" href="packages.html" title="">OUR RATES</a></li>
                                <li><a itemprop="url" href="contact.html" title="">CONTACT</a></li>
                                <li><a itemprop="url" href="about.html" title="">ABOUT US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blank"></div>
        </footer>		

    </div>

    <!-- Region Popup -->
    <div class="modal fade region" id="region" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/close.png" alt="" itemprop="image" /></span></button>
                    <div class="region-detail">
                        <div class="row">
                            <div class="col-md-6 column">
                                <div class="region-contact-info">
                                    <div class="heading2">
                                        <span>Fast And Safe</span>
                                        <h3>Office Address</h3>
                                    </div>
                                    <p>Transport logitec, ltd. 2258 millenioum Street Columbia, DK 85966</p>
                                    <div class="contact-detail">
                                        <span class="contact">
                                            <i class="fa fa-mobile"></i>
                                            <strong>Phone No</strong>
                                            <span>+858 5549 512</span>
                                            <span>+858 5549 512</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-email"></i>
                                            <strong>Email Address</strong>
                                            <span>info@transport@gmail.com</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-time"></i>
                                            <strong>Office Timing</strong>
                                            <span>10:00am - 06:00pm / Sunday: Close</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 column">
                                <div class="loc-thumb">
                                    <img src="https://placehold.it/340x222" alt="" itemprop="image" />
                                    <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit. Aennean commodo enean dolor sit amet, consectetuer.</p>
                                    <a class="theme-btn" href="#" title="" itemprop="url">СВЯЖИТЕСЬ С НАМИ СЕЙЧАС</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- Region Detail -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="submission-message" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="submission-data">
                        <span><img src="images/resource/submission.png" alt="" /></span>
                        <h1>СВЯЖИТЕСЬ С НАМИ</h1>
                        <p>СЛОГАН ИЛИ БЛАГОДАРНОСТЬ</p>
                        <a href="#" title="" class="theme-btn" data-dismiss="modal" aria-label="Close"><i class="fa fa-paper-plane"></i>ВЕРНУТЬСЯ</a>
                    </div><!-- Submission-data -->
                </div>
            </div>
        </div>
    </div>

    <!-- Script -->
    <script type="text/javascript" src="js/modernizr-2.0.6.js"></script><!-- Modernizr -->
    <script type="text/javascript" src="js/jquery-2.2.2.js"></script><!-- jQuery -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Bootstrap -->
    <script type="text/javascript" src="js/scrolltopcontrol.js"></script><!-- Scroll To Top -->
    <script type="text/javascript" src="js/jquery.scrolly.js"></script><!-- Scrolly -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Owl Carousal -->
    <script type="text/javascript" src="js/icheck.js"></script><!-- iCheck -->
    <script type="text/javascript" src="js/jquery.jigowatt.js"></script><!-- Contact Us -->
    <script type="text/javascript" src="js/select2.full.js"></script><!-- Select2 -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script><!-- Google Map -->
    <script type="text/javascript" src="js/perfect-scrollbar.js"></script><!-- Scroll Bar -->
    <script type="text/javascript" src="js/perfect-scrollbar.jquery.js"></script><!-- Scroll Bar -->
    <script src="js/script.js"></script>
    
    <script src="js/valid_form_register.js"></script>
    <script src="js/valid_form_auth.js"></script>


    <script>
        $(document).ready(function () {
            "use strict";

            //** Map **//
            function initialize() {
                var myLatlng = new google.maps.LatLng(51.5015588, -0.1276913);
                var mapOptions = {
                    zoom: 14,
                    disableDefaultUI: true,
                    scrollwheel: false,
                    center: myLatlng
                }
                var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                var image = 'images/icon.png';
                var myLatLng = new google.maps.LatLng(51.5015588, -0.1276913);
                var beachMarker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    icon: image
                });

            }
            google.maps.event.addDomListener(window, 'load', initialize);

        });

    </script>
</body>
</html>