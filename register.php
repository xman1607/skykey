<?php

	//Добавляем файл подключения к БД
	require_once("dbconnect.php");

	/*
	    Проверяем была ли отправлена форма, то есть была ли нажата кнопка зарегистрироваться. Если да, то идём дальше, если нет, значит пользователь зашёл на эту страницу напрямую. В этом случае выводим ему сообщение об ошибке.
	*/
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
		!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
		strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

		if(isset($_POST["first_name"])){
		    
		    //Обрезаем пробелы с начала и с конца строки
		    $first_name = trim($_POST["first_name"]);

		    //Проверяем переменную на пустоту
		    if(!empty($first_name)){
		        // Для безопасности, преобразуем специальные символы в HTML-сущности
		        $first_name = htmlspecialchars($first_name, ENT_QUOTES);
		    }else{

		    	$arr_result = [
		    	    'result_valid' => 'error',
		    	    'message' => 'Введите Ваше имя'
		    	];

		    	echo json_encode($arr_result);

		        //Останавливаем скрипт
		        exit();
		    }
		    
		}

		if(isset($_POST["last_name"])){

		    //Обрезаем пробелы с начала и с конца строки
		    $last_name = trim($_POST["last_name"]);

		    if(!empty($last_name)){
		        // Для безопасности, преобразуем специальные символы в HTML-сущности
		        $last_name = htmlspecialchars($last_name, ENT_QUOTES);
		    }else{

		    	$arr_result = [
		    	    'result_valid' => 'error',
		    	    'message' => 'Введите Вашу фамилию'
		    	];

		    	echo json_encode($arr_result);

		        //Останавливаем скрипт
		        exit();
		    }
		}

		if(isset($_POST["email"])){

		    //Обрезаем пробелы с начала и с конца строки
		    $email = trim($_POST["email"]);

		    if(!empty($email)){


		        $email = htmlspecialchars($email, ENT_QUOTES);

		        // (3) Место кода для проверки формата почтового адреса и его уникальности

		        //Проверяем формат полученного почтового адреса с помощью регулярного выражения
		        $reg_email = "/^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i";

		        //Если формат полученного почтового адреса не соответствует регулярному выражению
		        if( !preg_match($reg_email, $email)){
		            // Сохраняем в сессию сообщение об ошибке. 
		            
	            	$arr_result = [
	            	    'result_valid' => 'error',
	            	    'message' => 'Вы ввели Email в неправильно формате'
	            	];

	            	echo json_encode($arr_result);

	                //Останавливаем скрипт
	                exit();
		        }

		    }else{

	    		$arr_result = [
	    		    'result_valid' => 'error',
	    		    'message' => 'Введите Ваш Email'
	    		];

	    		echo json_encode($arr_result);

	    	    //Останавливаем скрипт
	    	    exit();

		    }

		}

		if(isset($_POST["password"])){

		    //Обрезаем пробелы с начала и с конца строки
		    $password = trim($_POST["password"]);

		    if(!empty($password)){
		        $password = htmlspecialchars($password, ENT_QUOTES);

		        //Шифруем папроль
		        $password = md5($password."top_secret"); 
		    }else{

		    	$arr_result = [
	    		    'result_valid' => 'error',
	    		    'message' => 'Введите Ваш пароль'
	    		];

	    		echo json_encode($arr_result);

	    	    //Останавливаем скрипт
	    	    exit();

		    }

		}

		//Запрос на добавления пользователя в БД
		$result_query_insert = $mysqli->query("INSERT INTO `users` (first_name, last_name, email, password) VALUES ('".$first_name."', '".$last_name."', '".$email."', '".$password."')");

		if(!$result_query_insert){
		    
		    $arr_result = [
    		    'result_valid' => 'error',
    		    'message' => 'Ошибка запроса на добавления пользователя в БД'
    		];

    		echo json_encode($arr_result);

    	    //Останавливаем скрипт
    	    exit();
		}else{

		    $arr_result = [
    		    'result_valid' => 'success',
    		    'message' => 'Регистрация прошла успешно!!! <br />Теперь Вы можете авторизоваться используя Ваш логин и пароль.'
    		];

    		echo json_encode($arr_result);

    	    //Останавливаем скрипт
    	    exit();
		}

		//Закрываем подключение к БД
		$mysqli->close();

	}else{
        exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=".$address_site."> главную страницу </a>.</p>");
    }
?>