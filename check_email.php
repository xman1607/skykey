<?php
    //Добавляем файл подключения к БД
    require_once("dbconnect.php");
     
    if(isset($_POST["email"])) {
 
        $email =  trim($_POST["email"]);
 
        $email = htmlspecialchars($email, ENT_QUOTES);
 
        //Проверяем, нет ли уже такого адреса в БД.
        $result_query = $mysqli->query("SELECT `email` FROM `users` WHERE `email`='".$email."'");
         
        //Если кол-во полученных строк ровно единице, значит, пользователь с таким почтовым адресом уже зарегистрирован
        if($result_query->num_rows == 1){

            $arr_result = [
                'result_valid' => 'error',
                'message' => 'Пользователь с таким почтовым адресом уже зарегистрирован'
            ];

            //echo "<span class='alert-danger'>Пользователь с таким почтовым адресом уже зарегистрирован</span>";

        }else{

            $arr_result = [
                'result_valid' => 'success',
                'message' => 'Почтовый адрес свободен'
            ];

            //echo "<span class='alert-success'>Почтовый адрес свободен</span>";
        }
        
        echo json_encode($arr_result);

        // закрытие выборки
        $result_query->close();
    }
?>