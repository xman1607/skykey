<?php
    /*
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);*/
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Sky Key</title>
<meta name="description" content="" />
<meta name="keywords" content="" />

<!-- Styles -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" /><!-- Bootstrap -->
<link rel="stylesheet" href="css/owl.carousel.css" type="text/css" /><!-- Owl Carousal -->	
<link rel="stylesheet" href="css/icons.css" type="text/css" /><!-- Font Awesome -->
<link rel="stylesheet" href="css/select2.min.css" type="text/css" /><!-- Select2 -->
<link rel="stylesheet" href="css/perfect-scrollbar.css" /><!-- Scroll Bar -->
<link rel="stylesheet" href="css/style.css" type="text/css" /><!-- Style -->	
<link rel="stylesheet" href="css/responsive.css" type="text/css" /><!-- Responsive -->		
<link rel="stylesheet" href="css/colors/color.css" type="text/css" /><!-- Color -->

</head>
<body itemscope="">
	<div class="theme-layout">
		
        <?php
            require_once("dbconnect.php");
            require_once("header.php");
        ?>
		


<?php
    if(isset($_SESSION["email"]) && isset($_SESSION["password"])){

        $email = $_SESSION["email"];
        $password = $_SESSION["password"];
?>
    
    

<?php

    $result_query = $mysqli->query("SELECT `id`, `first_name`, `last_name`, `email` FROM `users` WHERE `email`='".$email."' AND `password` = '".$password."'");

    if($result_query){

        $array_user_data = $result_query->fetch_array(MYSQLI_ASSOC);


        $customData = [
            'user_id' => $array_user_data['id'], 
            'first_name' => $array_user_data['first_name'],
            'last_name' => $array_user_data['last_name'],
            'user_email' => $array_user_data['email']
        ];

        //echo "jssonn customData = ".json_encode($customData);
?>
        <div class="page-top blackish overlape">
            <div class="parallax" data-velocity="-.1" style="background: url(https://placehold.it/1600x700) repeat scroll 0 0"></div>
            <div class="container">
                <div class="page-title">

                    <span>Понравившейся товар можно</span>
                    <h3>ЗАКАЗАТЬ СЕЙЧАС</h3>


                </div><!-- Page Title -->
            </div>
        </div><!-- Page Top -->




		<section class="block grayish">
			<div class="fixed-bg" style="background: url(https://placehold.it/1600x700) no-repeat scroll;"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="booking-page">
							<div class="row">
								<div class="col-md-4">
									<div class="person-img">
										<img src="/images/800х330.jpg" alt="" />
									</div>
								</div>
								<div class="col-md-8">
									<div class="booking">
										<div class="title2 title4">
											<strong>Оформить заказ</strong>
											<h2>ВВЕДИТЕ ВАШИ ДЕТАЛИ ЗАКАЗА</h2>
										</div>
										<P>Любой слоган</P>

										<div class="booking-form">

											<form action="https://www.sandbox.paypal.com/cgi-bin/websc" method="post" id="order_form" >
												<div class="row">
													<div class="col-md-6">
														<div class="select-from country select-box">
															<select id="country_send" name="country_send" required="required" placeholder="From country" >
                                                                <option value="" data-price="0"></option>
																<option value="usa" data-price="5"  >США</option>
																<option value="ca" data-price="10">Canada</option>
															</select>
														</div>
													</div>

													<div class="col-md-6">
														<div class="select-from city select-box">
															<select name="in_city">
                                                                <option value=""></option>
																<option value="city1">Paris</option>
                                                                <option value="city2">Kiev</option>
																<option value="city3">Moscow</option>
															</select>
														</div>
													</div>

													<div class="col-md-6">
														<div class="select-service select-box">
															<select name="service">
                                                                <option value=""></option>
																<option value="service_1">Service 1</option>
                                                                <option value="service_2">Service 2</option>
																<option value="service_3">Service 3</option>
																
															</select>
														</div>
													</div>

                                                    <div class="col-md-6">
                                                        <div class="select-goods select-box">
                                                            <select id="goods" name="goods" required="required">
                                                                <option value="" data-price="0"></option>
                                                                <option value="good_1" data-price="10">Good 1</option>
                                                                <option value="good_2" data-price="20">Good 2</option>
                                                                <option value="good_3" data-price="30">Good 3</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        
                                                        <input type="number" id="weight_package" name="weight_package" class="text-field" placeholder="Weight (kg)" min="1" value="1" required="required" />

                                                        <!-- <input type="text" id="weight_package" name="weight_package" value="1" class="text-field" placeholder="Weight (kg)" /> -->
                                                    </div>

													<div class="col-md-6">
														<div class="datepicker-field">
															<input class="datepicker" name="date" id="datepicker" type="text" placeholder="Выберите дату" required="required" />
														</div>
													</div>

                                                    <div class="col-md-12">
                                                        <div class="extra-services">
                                                            <h4><i class="fa fa-paper-plane"></i> БЫСТРАЯ ДОСТАВКА</h4>
                                                            <span>
                                                                <input tabindex="23" type="radio" name="delivery" value="express" id="field18" checked="checked" />
                                                                <label for="field18">Express Delivery</label>
                                                            </span>
                                                            <span>
                                                                <input tabindex="23" type="radio" name="delivery" value="1111" id="field19" />
                                                                <label for="field19">1111</label>
                                                            </span>
                                                            <span>
                                                                <input tabindex="23" type="radio" name="delivery" value="package" id="field110" />
                                                                <label for="field110">УПАКОВКА</label>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="cargo-size">
                                                            <h4><i class="fa fa-fire"></i>РАЗМЕР ПОСЫЛКИ:</h4>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="number" name="length_package" class="text-field" placeholder="Length (cm)" min="1" step="0.1" required="required" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="number" name="height_package" min="1" step="0.1" class="text-field" placeholder="Height (cm)" required="required" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>
                                                            <input type="text" class="text-field" name="first_name" placeholder="Введите имя" value="<?=$array_user_data['first_name']?>" required="required" />
                                                        </label>
                                                    </div>
                                                </div>
													

                                                    <div class="col-md-12">
                                                        <label>
                                                            <input type="text" class="text-field" name="last_name" placeholder="Введите фамилию" value="<?=$array_user_data['last_name']?>" required="required"/>
                                                        </label>
                                                    </div>

													<div class="col-md-6">
														<label>
                                                            <i class="fa fa-envelope"></i>
                                                            <input type="email" name="email" class="text-field" placeholder="Электроный адресс" value="<?=$array_user_data['email']?>" required="required" /> 
                                                        </label>
													</div>

													<div class="col-md-6">
														<label><i class="fa fa-phone"></i>
                                                            <input type="text" name="phone" class="text-field" placeholder="Номер телефона" required="required" /></label>
													</div>

													<div class="col-md-12">
														<label>
                                                            <input type="text" name="address" class="text-field" placeholder="Адресс" required="required" />
                                                        </label>
													</div>

													<div class="col-md-12">
														<textarea class="text-field" name="message" placeholder="Текст"></textarea>
													</div>

                                                    <div class="col-md-12" style="font-weight: bold;">
                                                        <p>Итоговая цена к оплате: <span id="final_price">0</span>$</p>
                                                    </div>

													<div class="col-md-12">
														<div class="terms-services">
															<span>
																<input tabindex="23" name="i_agree" type="checkbox" id="field116" required="required" />
																<label for="field116">Я согласен <a href="#" title="">с условиями</a> и <a href="#" title="">конфиденциальностью</a></label>
															</span>
														</div>
                                                        
                                                        <!-- Form from paypal https://www.paypal.com/cgi-bin/webscr?cmd=p/pdn/howto_checkout-outside -->

                                                        
                                                        <input type="hidden" name="cmd" value="_xclick">
                                                        <input type="hidden" name="business" value="sergiu1607-facilitator@gmail.com">

                                                        <input type="hidden" name="item_name" value="Order pay Description">
                                                        <input type="hidden" name="item_number" value="<?=$array_user_data['email']?>">
                                                        <input type="hidden" name="currency_code" value="USD">
                                                        <input type="hidden" name="amount" value="0">

                                                        <input type="hidden" name="cancel_return" value="<?=$address_site?>/paypal/payment_fail.php">

                                                        <input type="hidden" name="return" value="<?=$address_site?>/paypal/payment_success.php">

                                                        <input type="hidden" name="notify_url" value="<?=$address_site?>/paypal/ipn.php">

                                                        <input type="hidden" name="custom" 
                                                        value='<?=json_encode($customData)?>'>

                                                        <!-- <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but01.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!"> -->
                                                        <button type="submit">
                                                            Заказать сейчас
                                                        </button>

														<!-- <a title="" href="#" data-toggle="modal" data-target="#submission-message" class="theme-btn"><i class="fa fa-paper-plane"></i>Заказать сейчас</a> -->
													</div>
												</div>
											</form>
                                            </div>
                                        </div><!-- Calculate Shipping -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>  
            

        <?php

            }else{ //if($result_query){
        ?>

                <div class="page-top blackish overlape">
                    <div class="parallax" data-velocity="-.1" style="background: url(https://placehold.it/1600x700) repeat scroll 0 0"></div>
                    <div class="container">
                        <div class="page-title">

                            <p>
                               Ошибка запроса к базе данных.<br />
                               Описание ошибки: <?=$mysqli->error?>
                           </p>

                
                        </div><!-- Page Title -->
                    </div>
                </div><!-- Page Top -->
               
        <?php
            }
        ?>
                                            
<?php
    }else{
?>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="margin-bottom: 200px;">
                    <h2>Перед тем как сделать заказ, необходимо авторизироваться.</h2>
                </div>
            </div>
        </div>
<?php
    }
?>

		

		<footer>
			<section class="block">
				<div class="parallax dark" data-velocity="-.2" style="background: rgba(0, 0, 0, 0) url(https://placehold.it/1600x700) no-repeat 50% 0;"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3 column">
									<div class="widget">
										<div class="about-widget">
											<div class="logo">
												<a itemprop="url" href="index.html" title=""><img itemprop="image" src="images/resource/logo.png" alt="" /></a>
											</div>
											<div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Адрес доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Описание сервиса</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Таможенные лимиты</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Ограничения доставки</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Блог компании</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Расчет мощности LI-Ion АКБ</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            <ul class="social-btn">
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-facebook"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 column">
                                    <Div class="row">
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Условия доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Получение посылок</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Отслеживание посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Доставка негабарита</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Доставка с Амазона</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Ребейты</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>Услуги доставки</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="services-detail2.html" title="">ПЭ Стандарт</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail6.html" title="">Доставка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Отправка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail4.html" title="">Страхование посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Доставка с Ебей</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail.html" title="">Форум</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 column">
                                    <div class="widget blue1">
                                        <div class="heading2">
                                            <span>БЫСТРО И НАДЕЖНО</span>
                                            <h3>ОТПРАВИТЬ ПИСЬМО</h3>
                                        </div>
                                        <div class="subscription-form">
                                            <p itemprop="description">ЦИТАТА ИЛИ ЕЩЕ ЧТО ТО</p>
                                            <form>
                                                <input type="text" placeholder="Enter Your Email Address" />
                                                <a title="" href="#" class="theme-btn" data-toggle="modal" data-target="#submission-message"><i class="fa fa-paper-plane"></i>ОТПРАВИТЬ</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bottom-line">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 column">
                            <span>&copy; 2015 <a itemprop="url" title="" href="index.html">Unload</a> - All Rights Reserved - Made By Webinane</span>
                        </div>
                        <div class="col-md-6 column">
                            <ul>
                                <li><a itemprop="url" href="index.html" title="">HOME</a></li>
                                <li><a itemprop="url" href="services.html" title="">SERVICES</a></li>
                                <li><a itemprop="url" href="packages.html" title="">OUR RATES</a></li>
                                <li><a itemprop="url" href="contact.html" title="">CONTACT</a></li>
                                <li><a itemprop="url" href="about.html" title="">ABOUT US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blank"></div>
        </footer>		

    </div>

    <!-- Region Popup -->
    <div class="modal fade region" id="region" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/close.png" alt="" itemprop="image" /></span></button>
                    <div class="region-detail">
                        <div class="row">
                            <div class="col-md-6 column">
                                <div class="region-contact-info">
                                    <div class="heading2">
                                        <span>Fast And Safe</span>
                                        <h3>Office Address</h3>
                                    </div>
                                    <p>Transport logitec, ltd. 2258 millenioum Street Columbia, DK 85966</p>
                                    <div class="contact-detail">
                                        <span class="contact">
                                            <i class="fa fa-mobile"></i>
                                            <strong>Phone No</strong>
                                            <span>+858 5549 512</span>
                                            <span>+858 5549 512</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-email"></i>
                                            <strong>Email Address</strong>
                                            <span>info@transport@gmail.com</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-time"></i>
                                            <strong>Office Timing</strong>
                                            <span>10:00am - 06:00pm / Sunday: Close</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 column">
                                <div class="loc-thumb">
                                    <img src="https://placehold.it/340x222" alt="" itemprop="image" />
                                    <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit. Aennean commodo enean dolor sit amet, consectetuer.</p>
                                    <a class="theme-btn" href="#" title="" itemprop="url">СВЯЖИТЕСЬ С НАМИ СЕЙЧАС</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- Region Detail -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="submission-message" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="submission-data">
                        <span><img src="images/resource/submission.png" alt="" /></span>
                        <h1>СВЯЖИТЕСЬ С НАМИ</h1>
                        <p>СЛОГАН ИЛИ БЛАГОДАРНОСТЬ</p>
                        <a href="#" title="" class="theme-btn" data-dismiss="modal" aria-label="Close"><i class="fa fa-paper-plane"></i>ВЕРНУТЬСЯ</a>
                    </div><!-- Submission-data -->
                </div>
            </div>
        </div>
    </div>
	
<!-- Script -->
<script type="text/javascript" src="js/modernizr-2.0.6.js"></script><!-- Modernizr -->
<script type="text/javascript" src="js/jquery-2.2.2.js"></script><!-- jQuery -->
<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Bootstrap -->
<script type="text/javascript" src="js/scrolltopcontrol.js"></script><!-- Scroll To Top -->
<script type="text/javascript" src="js/jquery.scrolly.js"></script><!-- Scrolly -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Owl Carousal -->
<script type="text/javascript" src="js/icheck.js"></script><!-- iCheck -->
<script type="text/javascript" src="js/select2.full.js"></script><!-- Select2 -->
<script type="text/javascript" src="js/perfect-scrollbar.js"></script><!-- Scroll Bar -->
<script type="text/javascript" src="js/perfect-scrollbar.jquery.js"></script><!-- Scroll Bar -->
<script type="text/javascript" src="js/jquery.plugin.js"></script><!-- Calender -->
<script type="text/javascript" src="js/jquery.datepick.js"></script><!-- Calender -->
<script src="js/script.js"></script>
<script src="js/valid_form_register.js"></script>
<script src="js/valid_form_auth.js"></script>

<script>
$(document).ready(function(){
	'use strict';

	$('#datepicker').datepick();

    //====================== Initial price =======================
    //country_send
    var price_country_send = $('form#order_form #country_send option:selected').data('price');

    //wieght
    var price_weight;
    var count_weight = Number($('form#order_form #weight_package').val());
    if(count_weight == 1){
        price_weight = 20;
    }else{
        price_weight = 0;
    }
    
    

    
    //price good
    //var selected_good = $('form#order_form #goods option:selected');

    var price_good = $('form#order_form #goods option:selected').data('price');
    

    console.log('price_country_send = ', price_country_send);
    console.log('price_weight = ', price_weight);
    console.log('price_good = ', price_good);

    var final_price = price_country_send + price_weight + price_good;
    console.log('final_price = ', final_price);
    $('span#final_price').text(final_price);
    $('form#order_form input[name=amount]').val(final_price);


    //====================== change country_send =======================
    $('form#order_form #country_send').on('change', function(){

        //Получаем цену у выбранной страны отправки
        price_country_send = $(this).find(':selected').data('price');
        final_price = price_country_send + price_weight + price_good;
        console.log('final_price ( change country_send ) = ', final_price);
        $('span#final_price').text(final_price);
        $('form#order_form input[name=amount]').val(final_price);
    });

    //====================== change price_weight =======================

    $('form#order_form #weight_package').on('change', function(){

        count_weight = Number($(this).val());
        switch (true) {
            case count_weight == 1:
                price_weight = 20;
                break;
            case count_weight == 2:
                price_weight = 35;
                break;
            case count_weight == 3:
                price_weight = 50;
                break;
            case count_weight == 4:
                price_weight = 65;
                break;
            case count_weight == 5:
                price_weight = 80;
                break;
            case count_weight == 6:
                price_weight = 95;
                break;
            case count_weight == 7:
                price_weight = 110;
                break;
            case count_weight == 8:
                price_weight = 125;
                break;
            case count_weight == 9:
                price_weight = 140;
                break;
            case count_weight == 10:
                price_weight = 155;
                break;

            case count_weight > 10:
                price_weight = (count_weight - 1) * 10 + 65;
                break;
        }

        

        final_price = price_country_send + price_weight + price_good;
        //console.log('final_price ( change weight_package ) = ', final_price);
        $('span#final_price').text(final_price);
        $('form#order_form input[name=amount]').val(final_price);
    });

    //====================== change price good =======================
    
    $('form#order_form #goods').on('change', function(){

        //Получаем цену у выбранного товара
        price_good = $(this).find(':selected').data('price');

        if(price_good === 0){
            $('form#order_form #select2-goods-s4-container').css({
                'border': '1px solid #cc0000'
            });
        }else{
            $('form#order_form #select2-goods-s4-container').css({
                'border': 'none'
            });
        }

        final_price = price_country_send + price_weight + price_good;

        console.log('final_price ( change goods ) = ', final_price);

        if(!isNaN(final_price)){
            $('span#final_price').text(final_price);
            $('form#order_form input[name=amount]').val(final_price);
        }
        
    });


    /*$('form#order_form').on('submit', function(event){

        event.preventDefault();

        var selected_elements =  $('form#order_form select');

        $(selected_elements).each(function(index){

            var this_val = $(this).val();

            console.log('this val = ', this_val);

        });

        console.log('selected_elements = ', selected_elements.length);

    });*/

});
</script>
</body>
</html>