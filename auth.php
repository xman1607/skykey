<?php
    
    session_start();

    //Добавляем файл подключения к БД
    require_once("dbconnect.php");

    /*
        Проверяем была ли отправлена форма, то есть была ли нажата кнопка зарегистрироваться. Если да, то идём дальше, если нет, значит пользователь зашёл на эту страницу напрямую. В этом случае выводим ему сообщение об ошибке.
    */
    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && 
        !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

        if(isset($_POST["email"])){

            //Обрезаем пробелы с начала и с конца строки
            $email = trim($_POST["email"]);

            if(!empty($email)){
                $email = htmlspecialchars($email, ENT_QUOTES);

                //Проверяем формат полученного почтового адреса с помощью регулярного выражения
                $reg_email = "/^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i";

                //Если формат полученного почтового адреса не соответствует регулярному выражению
                if( !preg_match($reg_email, $email)){

                    $arr_result = [
                        'result_valid' => 'error',
                        'message' => 'Вы ввели почтовый адрес в неправильном формате.'
                    ];

                    echo json_encode($arr_result);

                    //Останавливаем скрипт
                    exit();
                }
            }else{

                $arr_result = [
                    'result_valid' => 'error',
                    'message' => 'Поле для ввода почтового адреса(email)<br /> не должна быть пустой'
                ];

                echo json_encode($arr_result);

                //Останавливаем скрипт
                exit();
            }
            

        }else{

            $arr_result = [
                'result_valid' => 'error',
                'message' => 'Отсутствует поле для ввода Email'
            ];

            echo json_encode($arr_result);

            //Останавливаем скрипт
            exit();
        }

        if(isset($_POST["password"])){

            //Обрезаем пробелы с начала и с конца строки
            $password = trim($_POST["password"]);

            if(!empty($password)){
                $password = htmlspecialchars($password, ENT_QUOTES);

                //Шифруем пароль
                $password = md5($password."top_secret");
            }else{
                $arr_result = [
                    'result_valid' => 'error',
                    'message' => 'Укажите Ваш пароль'
                ];

                echo json_encode($arr_result);

                //Останавливаем скрипт
                exit();
            }
            
        }else{

            $arr_result = [
                'result_valid' => 'error',
                'message' => 'Отсутствует поле для ввода пароля'
            ];

            echo json_encode($arr_result);

            //Останавливаем скрипт
            exit();
        }

        //Запрос в БД на выборке пользователя.
        $result_query_select = $mysqli->query("SELECT * FROM `users` WHERE email = '".$email."' AND password = '".$password."'");

        $array_user_data = $result_query_select->fetch_array(MYSQLI_ASSOC);

        if(!$result_query_select){
            $arr_result = [
                'result_valid' => 'error',
                'message' => 'Ошибка запроса на выборке пользователя из БД. <p>Код ошибки: '.$mysqli->errno.'</p><p>Описание ошибки: '.$mysqli->error.'</p>'
            ];

            echo json_encode($arr_result);
            //Останавливаем скрипт
            exit();

        }else{

            //Проверяем, если в базе нет пользователя с такими данными, то выводим сообщение об ошибке
            if($result_query_select->num_rows == 1){

                //======= Обработка галочки " запомнить меня " =======
                 
                //Проверяем, если галочка была поставлена
                if(isset($_POST["remember_me"])){
                 
                 
                    //Создаём токен
                    $password_cookie_token = md5($array_user_data["id"].$password.time());
                 
                    //Добавляем созданный токен в базу данных
                    $update_password_cookie_token = $mysqli->query("UPDATE users SET password_cookie_token='".$password_cookie_token."' WHERE email = '".$email."'");
                 
                    if(!$update_password_cookie_token){

                        $arr_result = [
                            'result_valid' => 'error',
                            'message' => 'Ошибка функционала запомнить меня. <p>Код ошибки: '.$mysqli->errno.'</p><p>Описание ошибки: '.$mysqli->error.'</p>'
                        ];

                        echo json_encode($arr_result);

                        //Останавливаем скрипт
                        exit();
                    }
                 
                    /* 
                        Устанавливаем куку.
                        Параметры функции setcookie():
                        1 параметр - Название куки
                        2 параметр - Значение куки
                        3 параметр - Время жизни куки. Мы указали 30 дней
                    */
                 
                    //Устанавливаем куку с токеном
                    setcookie("password_cookie_token", $password_cookie_token, time() + (1000 * 60 * 60 * 24 * 30));
                 
                }else{
                 
                    //Если галочка "запомнить меня" небыла поставлена, то мы удаляем куки
                    if(isset($_COOKIE["password_cookie_token"])){
                 
                        //Очищаем поле password_cookie_token из базы данных
                        $update_password_cookie_token = $mysqli->query("UPDATE users SET password_cookie_token = '' WHERE email = '".$email."'");
                 
                        //Удаляем куку password_cookie_token
                        setcookie("password_cookie_token", "", time() - 3600);
                    }
                     
                }
                
                // Если введенные данные совпадают с данными из базы, то сохраняем логин и пароль в массив сессий.
                $_SESSION['email'] = $email;
                $_SESSION['password'] = $password;

                $arr_result = [
                    'result_valid' => 'success',
                    'message' => ''
                ];

                echo json_encode($arr_result);

                //Останавливаем скрипт
                exit();

                //Возвращаем пользователя на главную страницу
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$address_site);

            }else{
                $arr_result = [
                    'result_valid' => 'error',
                    'message' => 'Неправильный логин и/или пароль'
                ];

                echo json_encode($arr_result);

                //Останавливаем скрипт
                exit();
            }
        }

    }else{
        exit("<p><strong>Ошибка!</strong> Вы зашли на эту страницу напрямую, поэтому нет данных для обработки. Вы можете перейти на <a href=".$address_site."> главную страницу </a>.</p>");
    }
?>