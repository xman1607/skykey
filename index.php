<?php
    
    /*// Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    //Load composer's autoloader
    require 'vendor/autoload.php';
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.skykey.uz';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'admin@skykey.uz';                 // SMTP username
        $mail->Password = '6&auNX3P@ehg';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('admin@skykey.uz', 'Mailer');
        $mail->addAddress('xmantest3@gmail.com', 'Joe User');     // Add a recipient
        $mail->addReplyTo('admin@skykey.uz', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Test letter';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';

    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }*/

?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sky Key</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />

<!-- Styles -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" /><!-- Bootstrap -->
<link rel="stylesheet" href="css/owl.carousel.css" type="text/css" /><!-- Owl Carousal -->	
<link rel="stylesheet" href="css/icons.css" type="text/css" /><!-- Font Awesome -->
<link rel="stylesheet" href="css/select2.min.css" type="text/css" /><!-- Select2 -->
<link rel="stylesheet" href="css/perfect-scrollbar.css" /><!-- Scroll Bar -->
<link rel="stylesheet" href="css/style.css" type="text/css" /><!-- Style -->	
<link rel="stylesheet" href="css/responsive.css" type="text/css" /><!-- Responsive -->		
<link rel="stylesheet" href="css/colors/color.css" type="text/css" /><!-- Color -->	
<link rel="stylesheet" href="layerslider/css/layerslider.css" type="text/css"><!-- Layer Slider -->

</head>
<body itemscope="">
    <div class="theme-layout">
        

        <?php
            require_once("header.php");
        ?>

        <div class="page-top blackish overlape">
            <div class="parallax" data-velocity="-.1" style="background: url(https://placehold.it/1600x700) repeat scroll 0 0"></div>
            <div class="container">
                <div class="page-title">
                    <span>ИДЕАЛЬНО ДЛЯ ВАС</span>
                    <h3>ГРУЗОВЫЕ ПЕРЕВОЗКИ</h3>
                </div><!-- Page Title -->
            </div>
        </div><!-- Page Top -->

        <section class="block gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<h2 class="style-title">КАК ЭТО РАБОТАЕТ?</h2>
                        <div class="row">
                        	<div class="col-md-3">
                        		<div class="modern-service">
	                                <div class="mod-service-inner">
	                                    <span><img itemprop="image" alt="" src="images/resource/101.png"></span>
	                                    <h3><a itemprop="url" href="#" title="" class="popup1">ЗАРЕГИСТРИРУЙТЕСЬ</a></h3>
	                                   
	                                    <p>и получите личный адрес у нас на складе. Это займет 5 минут.</p>
	                                </div>
	                            </div><!-- Modern Service -->
                        	</div>

                        	<div class="col-md-3">
                        		<div class="modern-service">
	                                <div class="mod-service-inner">
	                                    <span><img itemprop="image" alt="" src="images/resource/102.png"></span>
	                                    <h3><a href="#" title="">ЗАКАЖИТЕ ТОВАР</a></h3>
	                                    
	                                    <p>в любом магазине США или Канады, указав полученный адрес для доставки.</p>
	                                </div>
	                            </div><!-- Modern Service -->
                        	</div>

                        	<div class="col-md-3">
                        		<div class="modern-service">
	                                <div class="mod-service-inner">
	                                    <span><img itemprop="image" alt="" src="images/resource/103.png"></span>
	                                    <h3><a href="#" title="">ОПЛАТИТЕ ДОСТАВКУ</a></h3>
	                                    
	                                    <p>товара любой пластиковой картой, когда заказ придет к нам на склад.</p>
	                                </div>
	                            </div><!-- Modern Service -->
                        	</div>

                        	<div class="col-md-3">
                        		<div class="modern-service">
	                                <div class="mod-service-inner">
	                                    <span><img itemprop="image" alt="" src="images/resource/101.png"></span>
	                                    <h3><a href="#" title="">ПОЛУЧИТЕ ПОКУПКУ</a></h3>
	                                    
	                                    <p>в течении 6-15 дней курьер доставит посылку до Вашей двери</p>
                                        
	                                </div>
	                            </div><!-- Modern Service -->
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>	

        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="about-shipment">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="safe-affordable-cargo">
                                        <div class="title2">
                                            <strong><i class="fa fa-steam"></i>ПРИИМУЩЕСТВА</strong>
                                            <h2>СТРАХОВКА НА 100$ УЖЕ ВКЛЮЧЕНА В СТОИМОСТЬ ПЕРЕСЫЛКИ</h2>
                                        </div>
                                        <p itemprop="description">У нас прямые контракты на доставку посылок с ЕМС, Почтой УЗБЕКИСТАНА, СПСР. Никаких посредников! Поэтому мы предлагаем для наших клиентов самые низкие цены по доставке/p>
                                        <div class="services1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="simple-services1">
                                                        <div class="service-box1">
                                                            <img src="images/resource/77.png" alt="" />
                                                            <h5 class="counter">2257</h5>
                                                            <span>ЗАКАЗЫ</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="simple-services1">
                                                        <div class="service-box1">
                                                            <img src="images/resource/88.png" alt="" />
                                                            <h5 class="counter">6919</h5>
                                                            <span>ДОСТАВЛЕНО</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="theme-btn dark" href="about.html" title=""><i class="fa fa-paper-plane"></i>  О НАС ПОДРОБНО</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="toggle toggle-style4" id="toggle4">
                                        <div class="toggle-item activate">
                                            <h3 class="active"><i class="fa fa-life-bouy"></i>Доставка товаров и посылок из США</h3>
                                            <div class="content">
                                                <div class="simple-text">
                                                    <p>Компания Sky Key предлагает услуги посредника для покупок товаров из США, Канады. Мы осуществляем доставку посылок и грузов из США.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="toggle-item">
                                            <h3 class=""><i class="fa fa-joomla"></i>Почему МЫ?</h3>
                                            <div class="content">
                                                <div class="simple-text">
                                                    <p>Большинство людей может самостоятельно купить понравившуюся вещь на Амазоне или Ебей, но вот как доставить покупку - это большой вопрос.
Посылку нужно консолидировать, ведь не будете же вы отправлять все покупки по отдельности. Кроме того, хочется быть уверенным, что все дойдет в целости и сохранности, а главное - в срок!
Именно этим мы и занимаемся! Мы предоставляем вам персональный склад в США, принимаем все ваши покупки из любого интернет-магазина, проверяем их целостность, собираем всё в одну посылку и за одну неделю доставляем до вашей двери! Вам не нужно больше ломать голову над оформлением заказа и перелопачивать горы информации. С Sky Key доставка из США стала делом нескольких шагов.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="toggle-item">
                                            <h3 class=""><i class="fa fa-leaf"></i>ЗАРЕГИСТРИРУЙТЕСЬ НА САЙТЕ</h3>
                                            <div class="content">
                                                <div class="simple-text">
                                                    <p>И получите персональный виртуальный адрес в США</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="toggle-item">
                                            <h3 class=""><i class="fa fa-magic"></i>Виртуальный адрес в США?</h3>
                                            <div class="content">
                                                <div class="simple-text">
                                                    <p>Закажите товар в любом интернет-магазине США, Канады, указав полученный виртуальный адрес в США и получите посылку в кратчайшие сроки . Вам не нужно будет обращаться к третим лицам. Оплатили посылка пришла на Ваш виртуальный адресс с которого направится к Вам в руки</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="toggle-item">
                                            <h3 class=""><i class="fa fa-microphone"></i>Прибытие на склад</h3>
                                            <div class="content">
                                                <div class="simple-text">
                                                    <p>Как только все посылки придут на склад - объедините их в одну и оплатите доставку</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- About Shipment -->
                    </div>
                </div>
            </div>
        </section>

        

        <section class="block gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <span><i class="fa fa-steam"></i>БЕЗОПАСНО И БЫСТРО</span>
                            <h2>О нас</h2>
                        </div>
                        <div class="overview-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="tabs2 tabs-styles">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tabs2-tab1" data-toggle="tab"><i class="fa fa-life-bouy"></i> КАК МЫ РАБОТАЕМ?</a></li>
                                            <li><a href="#tabs2-tab2" data-toggle="tab"><i class="fa fa-leaf"></i> О доставке</a></li>
                                            <li><a href="#tabs2-tab3" data-toggle="tab"><i class="fa fa-magic"></i> Доставка</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tabs2-tab1">
                                                <p>Воспользовавшись услугами компании Sky Key, Вы получаете возможность покупать товары в интернет магазинах и на интернет аукционах Америки и Канады, даже если продавец не осуществляет доставку в Ваш город</p>
                                            </div>
                                            <div class="tab-pane fade" id="tabs2-tab2">
                                                <p>Зарегистрировавшись на нашем сайте Вы получаете индивидуальный личный кабинет и личный адрес в Америке и Канаде, которые Вы можете использовать при покупках. Совершив покупку в Америке, Вы в личном кабинете указываете название интернет магазина, описание содержимого посылки, стоимость каждого товара и примерный вес. В момент доставки, вам на адрес электронной почты, который вы указали при регистрации, придет уведомление о том что она находится на нашем специально оборудованном складе. В дальнейшем Вы можете объединить несколько товаров из разных магазинов в одну большую посылку, выбрать наиболее подходящий Вам метод доставки, и отправить сборную посылку к себе. При желании Вы конечно можете не объединять посылки, а отправить любую отдельно взятую без переупаковки, или например разделить одну поступившую на наш склад посылку на несколько более мелких.

Мы предлагаем Вам воспользоваться нашим калькулятором стоимости доставки чтобы получить примерную стоимость доставки посылки, в зависимости от веса, пункта назначения, и способа доставки.</p>
                                            </div>
                                            <div class="tab-pane fade" id="tabs2-tab3">
                                                <p>Компания Скай Кей доставляет товары из Америки и Канады в 150 стран мира, включая страны СНГ.
Мы предлагаем Вам использовать при покупках в интернет-магазинах и на интернет-аукционах (включая Amazon и Ebay) в качестве Вашего адреса доставки любой из наших складов, в том числе в безналоговом штате Делавер (что позволит вам сэкономить на sales taxes – налогах на продажу) и Канады.

Мы рады Вам предложить самую низкую стоимость доставки из Америки за счет того, что компания имеет собственную логистику и контрактные цены со многими службами доставки.
На нашем сайте, воспользовавшись удобным виртуальным калькулятором, Вы можете самостоятельно подсчитать сколько будет стоить доставка из Америки, однако мы обращаем Ваше внимание на то, что Вы увидите предварительную стоимость доставки. Окончательную стоимость доставки можно будет определить после того, как посылка будет сформирована на складе. 

Компания Sky Key доставляет посылки своей логистикой в Россию и Узбекистан. При доставке методом ПЭ-Стандарт маршрут исключает остановки в транзите. Однако для оформления доставки ПЭ-Стандарт необходимо соблюдать правила оформления документов, с подробным описанием этих правил Вы можете ознакомиться на странице.
По желанию клиента возможна доставка почтовой службой USPS (EMS, Priority, Airmail) или международными курьерскими компаниями.

Вы сами выбираете как Ваши покупки будут Вам доставлены. Возможна почтовая и курьерская доставка. При почтовой доставке Вы самостоятельно забираете свою посылку в Вашем местном почтовом отделении, при курьерской доставке Вашу посылку к Вам на дом привезёт курьер.

Мы самостоятельно и совершенно бесплатно оформляем таможенные и иные документы которые должны быть у посылки, согласно международным нормативам тех стран, куда она будет отправлена.

При отправке все посылки страхуются. Страхование посылки на $100.00 уже включено в стоимость доставки.
Мы бесплатно предоставляем сервис отслеживания местонахождения посылки.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="cargo-shipment">
                        <div class="calculate-shipping style2">
                            <div class="dark-title">
                                <span><i class="fa fa-steam"></i>Удобно и быстро</span>
                                <h3>КАЛЬКУЛЯТОР ПОГРУЗКИ</h3>
                            </div>

                            <div class="calculate-shipping-form">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="select-service select-box">
                                                <select>
                                                    <option>Выбрать сервис</option>
                                                    <option>Air</option>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="select-from select-box">
                                                <select>
                                                    <option>Из</option>
                                                    
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="select-goods select-box">
                                                <select>
                                                    <option>Товар</option>
                                                    
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="select-to select-box">
                                                <select>
                                                    <option>Куда</option>
                                                    
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <input type="text" class="text-field" placeholder="Вес (kg)" />
                                        </div>

                                        <div class="col-md-6">
                                            <div class="extra-services">
                                                <h4><i class="fa fa-paper-plane"></i> ДОПОЛНИТЕЛЬНО</h4>
                                                <span>
                                                    <input tabindex="23" type="checkbox" id="field18" />
                                                    <label for="field18">БЫСТРАЯ ДОСТАВКА</label>
                                                </span>
                                                <span>
                                                    <input tabindex="23" type="checkbox" id="field19" />
                                                    <label for="field19">ЕЩЕ ЧТО ТО</label>
                                                </span>
                                                <span>
                                                    <input tabindex="23" type="checkbox" id="field110" />
                                                    <label for="field110">Упаквка</label>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="cargo-size">
                                                <h4><i class="fa fa-fire"></i>РАЗМЕР:</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="text-field" placeholder="Ширина (cm)" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="text-field" placeholder="Высота (cm)" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <a href="#" title="" class="theme-btn"><i class="fa fa-paper-plane"></i> ПРОВЕРИТЬ</a>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="total">
                                                <div class="cargo-total"><h4>ЦЕНА:</h4> $345</div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div><!-- Calculate Shipping -->
                    </div><!-- Cargo Shipment -->
                </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>                                       
                    </div>
                </div>
            </div>
        </section>

        

        <section class="block no-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="partners" id="partners">
                            <li><a itemprop="url" href="#" title=""><img itemprop="image" src="images/amazon.jpg" alt="" /></a></li>
                            <li><a itemprop="url" href="#" title=""><img itemprop="image" src="images/ebey.jpg" alt="" /></a></li>
                            <li><a itemprop="url" href="#" title=""><img itemprop="image" src="images/gap.jpg" alt="" /></a></li>
                            <li><a itemprop="url" href="#" title=""><img itemprop="image" src="images/zappos.jpg" alt="" /></a></li>
                            <li><a itemprop="url" href="#" title=""><img itemprop="image" src="images/ebey.jpg" alt="" /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>		

        <footer>
            <section class="block">
                <div class="parallax dark" data-velocity="-.2" style="background: rgba(0, 0, 0, 0) url(https://placehold.it/1600x700) no-repeat 50% 0;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 column">
                                    <div class="widget">
                                        <div class="about-widget">
                                            <div class="logo">
                                                <a itemprop="url" href="index.html" title=""><img itemprop="image" src="images/resource/logo.png" alt="" /></a>
                                            </div>
                                           <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Адрес доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Описание сервиса</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Таможенные лимиты</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Ограничения доставки</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Блог компании</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Расчет мощности LI-Ion АКБ</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            <ul class="social-btn">
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-facebook"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 column">
                                    <Div class="row">
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Условия доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Получение посылок</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Отслеживание посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Доставка негабарита</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Доставка с Амазона</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Ребейты</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>Услуги доставки</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="services-detail2.html" title="">ПЭ Стандарт</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail6.html" title="">Доставка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Отправка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail4.html" title="">Страхование посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Доставка с Ебей</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail.html" title="">Форум</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 column">
                                    <div class="widget blue1">
                                        <div class="heading2">
                                            <span>БЫСТРО И НАДЕЖНО</span>
                                            <h3>ОТПРАВИТЬ ПИСЬМО</h3>
                                        </div>
                                        <div class="subscription-form">
                                            <p itemprop="description">ЦИТАТА ИЛИ ЕЩЕ ЧТО ТО</p>
                                            <form>
                                                <input type="text" placeholder="Enter Your Email Address" />
                                                <a title="" href="#" class="theme-btn" data-toggle="modal" data-target="#submission-message"><i class="fa fa-paper-plane"></i>ОТПРАВИТЬ</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bottom-line">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 column">
                            
                        </div>
                        <div class="col-md-6 column">
                            <ul>
                                <li><a itemprop="url" href="index.html" title="">ГЛАВНАЯ</a></li>
                                <li><a itemprop="url" href="services.html" title="">СЕРВИС</a></li>
                                <li><a itemprop="url" href="packages.html" title="">ЗАКАЗАТЬ</a></li>
                                <li><a itemprop="url" href="contact.html" title="">МАГАЗИНЫ</a></li>
                                <li><a itemprop="url" href="about.html" title="">КОНТАКТЫ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blank"></div>
        </footer>		

    </div>

    <!-- Region Popup -->
    <div class="modal fade region" id="region" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/close.png" alt="" itemprop="image" /></span></button>
                    <div class="region-detail">
                        <div class="row">
                            <div class="col-md-6 column">
                                <div class="region-contact-info">
                                    <div class="heading2">
                                        <span>Быстро и Сохрано </span>
                                        <h3>Адресс</h3>
                                    </div>
                                    <p>444</p>
                                    <div class="contact-detail">
                                        <span class="contact">
                                            <i class="fa fa-mobile"></i>
                                            <strong>Phone No</strong>
                                            <span>1917-653-80702</span>
                                            <span>1917-653-8070</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-email"></i>
                                            <strong>Email Address</strong>
                                            <span>info@transport@gmail.com</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-time"></i>
                                            <strong>Время работы</strong>
                                            <span>10:00am - 06:00pm / Воскресение: Выходной</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 column">
                                <div class="loc-thumb">
                                    <img src="https://placehold.it/340x222" alt="" itemprop="image" />
                                    <p>444.</p>
                                    <a class="theme-btn" href="#" title="" itemprop="url">СВЯЖИТЕСЬ С НАМИ СЕЙЧАС</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- Region Detail -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="submission-message" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="submission-data">
                        <span><img src="images/resource/submission.png" alt="" /></span>
                        <h1>СВЯЖИТЕСЬ С НАМИ</h1>
                        <p>СЛОГАН ИЛИ БЛАГОДАРНОСТЬ</p>
                        <a href="#" title="" class="theme-btn" data-dismiss="modal" aria-label="Close"><i class="fa fa-paper-plane"></i>ВЕРНУТЬСЯ</a>
                    </div><!-- Submission-data -->
                </div>
            </div>
        </div>
    </div>

<!-- Script -->
<script type="text/javascript" src="js/modernizr-2.0.6.js"></script><!-- Modernizr -->
<script type="text/javascript" src="js/jquery-2.2.2.js"></script><!-- jQuery -->
<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Bootstrap -->
<script type="text/javascript" src="js/scrolltopcontrol.js"></script><!-- Scroll To Top -->
<script type="text/javascript" src="js/jquery.scrolly.js"></script><!-- Scrolly -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Owl Carousal -->
<script type="text/javascript" src="js/icheck.js"></script><!-- iCheck -->
<script type="text/javascript" src="js/select2.full.js"></script><!-- Select2 -->
<script type="text/javascript" src="js/jquery.counterup.min.js"></script><!-- CounterUp -->
<script type="text/javascript" src="js/waypoints.js"></script><!-- Waypoints -->
<script type="text/javascript" src="js/jquery.poptrox.min.js"></script><!-- LightBox -->
<script type="text/javascript" src="js/perfect-scrollbar.js"></script><!-- Scroll Bar -->
<script type="text/javascript" src="js/perfect-scrollbar.jquery.js"></script><!-- Scroll Bar -->
<script src="js/script.js"></script>

<script src="js/valid_form_register.js"></script>
<script src="js/valid_form_auth.js"></script>

<script>
    $(document).ready(function () {
        "use strict";

        //** Main Img Carousel  **//
        $("#main-img-carousel").owlCarousel({
            autoplay: true,
            autoplayTimeout: 2000,
            smartSpeed: 1500,
            loop: true,
            dots: false,
            nav: false,
            margin: 10,
            items: 1,
            singleItem: true,
        });

        //** Counter Up **//
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        $("#modern-services-carousel").owlCarousel({
            autoplay: true,
            autoplayTimeout: 3000,
            smartSpeed: 2000,
            loop: false,
            dots: false,
            nav: true,
            margin: 30,
            items: 3,
            responsiveClass: true,
            responsive: {
                1200: {items: 3},
                980: {items: 2},
                480: {items: 2},
                0: {items: 1}
            }
        });

        //** Company Projects **//
        $("#company-projects-list").addClass("loaded");

        var l = $("#company-projects-list > ul li").length;
        for (var i = 0; i <= l; i++) {
            var room_list = $("#company-projects-list > ul li").eq(i);
            var room_img_height = $(room_list).find(".company-project > img").innerHeight();
            $(room_list).css({
                "height": room_img_height
            });
            $(room_list).find(".company-project > img").css({
                "width": "100%"
            });
        }

        $("#company-projects-list > ul li.start").addClass("active");
        $("#company-projects-list > ul li").on("mouseenter", function () {
            $("#company-projects-list > ul li").removeClass("active");
            $(this).addClass("active");
        });

        // Accordion //
        $('#toggle4 .content').hide();
        $('#toggle4 h3:first').addClass('active').next().slideDown(500).parent().addClass("activate");
        $('#toggle4 h3').on("click", function () {
            if ($(this).next().is(':hidden')) {
                $('#toggle4 h3').removeClass('active').next().slideUp(500).removeClass('animated zoomIn').parent().removeClass("activate");
                $(this).toggleClass('active').next().slideDown(500).addClass('animated zoomIn').parent().toggleClass("activate");
                return false;
            }
        });

        /*=================== LightBox ===================*/
        var foo = $('.lightbox');
        foo.poptrox({
            usePopupCaption: true,
            usePopupNav: true,
        });
    });
</script>
</body>
</html>