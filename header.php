<?php
    session_start();
?>
<header class="fancy-header stick">
    <div class="top-sec">
        <div class="top-bar">
            <div class="container">
                <span class="cargo-time"><i class="fa fa-clock-o"></i>Рабочее время: 08:00AM - 9:00PM</span>
                <div class="connect-us">
                    <ul class="social-btn">
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-facebook"></i></a></li>
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-reddit"></i></a></li>
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>

                <div class="extra-links">
                    <a itemprop="url" href="#" title="">Support</a>   /   
                    
                    <?php
                        if(!isset($_SESSION["email"]) && !isset($_SESSION["password"])){
                    ?>

                    <a itemprop="url" href="#" title="" class="popup1">Войти</a>
                    
                    <?php 
                        }else{
                    ?>
                            <a href="/logout.php">Выход</a>
                            <p>ВЫ АВТОРИЗОВАНЫ</p>
                    <?php
                        }
                    ?>
                    
                </div>
            </div>
        </div>
    </div><!-- Top Sec -->
    <div class="logo-menu-sec">
        <div class="logo-menu">
            <div class="logo">
                <a itemprop="url" href="/" title=""><img itemprop="image" src="images/resource/logo.png" alt="" /></a>
            </div>
            <div class="quick-contact">
                <ul>
                    <li>
                        <img src="images/resource/phone.png" alt="" />
                        <span>1917-653-8070</span>
                        <p>Ташкентский офис</p>
                    </li>
                    <li>
                        <img src="images/resource/sms.png" alt="" />
                        <span>klyuchinskiy@gmail.com</span>
                        <p>Задать вопрос?</p>
                    </li>
                    <li>
                        <a href="/order-now.php" title="" itemprop="url" class="theme-btn">ЗАКАЗАТЬ</a>
                    </li>
                </ul>
            </div>
        </div>
        <nav class="menu-curve">
            <ul>
                <li class="menu-item-has-children">
                    <a itemprop="url" href="/" title="">ГЛАВНАЯ</a>
                </li>
                <li class="menu-item-has-children"><a itemprop="url" href="/services-detail.php" title="">СЕРВИС</a>
                    
                <!-- <li class="menu-item-has-children mirror"><a itemprop="url" href="/order-now.php" title="">ЗАКАЗАТЬ</a> -->
                
                
                <li class="menu-item-has-children"><a itemprop="url" href="/gallery4.php" title="">МАГАЗИНЫ</a>
                   
               
                <li><a itemprop="url" href="/contact.php" title="">КОНТАКТЫ</a></li>
            </ul>
        </nav>
    </div><!-- Logo Menu Sec -->
</header>

<div class="responsive-header">
    <span class="top-sec-btn"><i class="fa fa-angle-double-down"></i></span>
    <div class="responsive-top-sec">
        <div class="responsive-top-bar top-bar">
            <div class="container">
                <span class="cargo-time">Время открытия :<i>08:00AM - 9:00PM</i></span>
                <div class="connect-us">
                    <ul class="social-btn">
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-facebook"></i></a></li>
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a itemprop="url" href="#" title=""><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- Responsive Top Bar -->
        <div class="responsive-quick-contact">
            <div class="container">
                <div class="quick-contact">
                    <ul>
                        <li>
                            <img src="images/resource/phone.png" alt="" />
                            <span>1917-653-8070</span>
                            <p>Офис в Ташкенте</p>
                        </li>
                        <li>
                            <img src="images/resource/sms.png" alt="" />
                            <span>klyuchinskiy@gmail.com</span>
                            <p>Разные вопросы?</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- Responsive Quick Contact -->
    </div>
    <div class="responsive-nav">
        <div class="container">
            <div class="responsive-logo">
                <div class="logo">
                    <a itemprop="url" href="/" title=""><img itemprop="image" src="images/resource/logo.png" alt="" /></a>
                </div>
            </div>
            <span class="responsive-btn"><i class="fa fa-list"></i></span>
            <div class="responsive-menu">
                <span class="close-btn"><i class="fa fa-close"></i></span>
                <ul class="responsive-popup-btns">
                    <li><i class="fa fa-user"></i><a itemprop="url" href="#" title="" class="popup1">ВОЙТИ</a></li>
                    <li><i class="fa fa-paper-plane"></i><a itemprop="url" href="/order-now.php" title="" class="popup2">ЗАКАЗАТЬ</a></li>
                </ul>
                <ul>
                    <li class="menu-item-has-children"><a itemprop="url" href="/" title="">ГЛАВНАЯ</a>
                        
                    </li>
                    <li class="menu-item-has-children"><a itemprop="url" href="/services-detail.php" title="">СЕРВИС</a>
                       
                    </li>
                    <!-- <li class="menu-item-has-children"><a itemprop="url" href="/order-now.php" title="">ЗАКАЗАТЬ</a> -->
                        
                    <li class="menu-item-has-children"><a itemprop="url" href="/gallery4.php" title="">МАГАЗИНЫ</a>
                        
                    <li><a itemprop="url" href="/contact.php" title="">КОНТАТЫ</a></li>
                </ul>
            </div><!-- Responsive Menu -->
        </div>
    </div>
</div><!--Responsive header-->

<div id="signup-popup">
    <div class="region2"  id="signup">
        <div class="modal-dialog1">
            <div class="modal-content1">
                <div class="modal-body1">
                    <div class="signup-form">
                        <button type="submit"><img src="images/close1.png" alt="" /></button>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="sign-in banner-detail1">
                                    <div class="heading2">
                                        <span>эээ</span>
                                        <h3>Войти</h3>
                                    </div>
                                    <p>4444</p>
                                    <form action="auth.php" method="POST" id="form_auth" >
                                        <label>
                                            <i class="fa fa-envelope"></i>
                                            <input type="text" name="email" class="text-field" placeholder="Введите почтовый адрес" required>
                                        </label>
                                        <label>
                                            <i class="fa fa-anchor"></i>
                                            <input type="password" name="password" class="text-field" minlength="6" placeholder="Пороль:" required >
                                        </label>
                                        <div class="terms-services">
                                            <span>
                                                <input tabindex="23" type="checkbox" id="field15" name="remember_me" />
                                                <label for="field15">Запомнить пороль</label>
                                            </span>
                                        </div>
                                        <p class="note_message text-center"></p>
                                        <ul>
                                            <li>
                                                <!-- <input type="submit" name="btn_submit_auth" value="ВОЙТИ СЕЙЧАС" /> -->
                                                <a href="#" title="" class="theme-btn link_submit_auth" itemprop="url">
                                                    <i class="fa fa-paper-plane"></i>ВОЙТИ СЕЙЧАС
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="" itemprop="url">Забыть Пороль</a>
                                            </li>
                                        </ul>
                                    </form>
                                </div><!-- Sign In -->
                            </div>

                            <div class="col-md-6">
                                <div class="sign-in banner-detail1 si">
                                    <div class="heading2">
                                        <span>11e</span>
                                        <h3>Зарегистрироваться</h3>
                                    </div>
                                    <p>111</p>
                                    <form action="register.php" method="POST" id="form_register">
                                        <label>
                                            <input type="text" name="last_name" class="text-field" placeholder="Фамилия" required="required" >
                                        </label>
                                        <label>
                                            <input type="text" name="first_name" class="text-field" placeholder="Имя" required="required">
                                        </label>
                                        <label>
                                            <i class="fa fa-envelope"></i>
                                            <input type="email" name="email" class="text-field" placeholder="Электроный адресс" required="required">
                                        </label>
                                        <label>
                                            <i class="fa fa-anchor"></i>
                                            <input type="password" name="password" class="text-field" placeholder="Новый пороль: " required="required" minlength="6" maxlength="191">
                                        </label>
                                        <label>
                                            <i class="fa fa-anchor"></i>
                                            <input type="password" name="confirm_password" class="text-field" placeholder="Повторить пороль:" required="required">
                                        </label>
                                        <div class="terms-services">
                                            <span>
                                                <input tabindex="23" name="i_agree" type="checkbox" id="field16" required="required" />
                                                <label for="field16">Я согласен <a href="#" title="">с Условиями</a> и <a href="#" title="">Конфиденциальностью</a></label>
                                            </span>
                                        </div>
                                        <!-- <div>
                                            <input type="submit" name="btn_submit_register" value="ВОЙТИ СЕЙЧАС" />
                                        </div> -->
                                        <!-- Место для вывода сообщений -->
                                        <p class="note_message text-center"></p>

                                        <a href="#" id="link_submit_register" class="theme-btn" itemprop="url">
                                            <i class="fa fa-paper-plane"></i>ВОЙТИ СЕЙЧАС
                                        </a>
                                    </form>
                                </div><!-- Sign In -->
                            </div>
                        </div>
                    </div><!-- Signup Form -->
                </div>
            </div>
        </div>
    </div>
</div><!-- Signup Popup -->