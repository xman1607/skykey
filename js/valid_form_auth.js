$(document).ready(function () {
    "use strict";

    //================ Проверка email ==================

    //регулярное выражение для проверки email
    var email_validation;
    var pattern_email = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    var form_auth_note = $('form#form_auth .note_message');
    
    $('form#form_auth input[name=email]').blur(function(){
        var email_val = $(this).val();

        if(email_val.length > 0){

            email_validation = pattern_email.test(email_val);

            if(!email_validation){
                

                $(this).css({'border-bottom':'1px solid #cc0000'});

                form_auth_note.html("Вы ввели почтовый адрес в неправильном формате.");
                form_auth_note.addClass('alert-danger');

                $('form#form_auth a.link_submit_auth').css('background', '#ccc');
                $('form#form_auth a.link_submit_auth').css('pointer-events', 'none');

            }else{
                $(this).css({'border-bottom':'none'});

                form_auth_note.empty();
                form_auth_note.removeClass('alert-danger');

                $('form#form_auth a.link_submit_auth').css('background', '#ffb400');
                $('form#form_auth a.link_submit_auth').css('pointer-events', 'auto');
            }

        //После очистки ввода, нужно активировать кнопку
        }else{
            email_validation = false;
            $(this).css({'border-bottom':'1px solid #cc0000'});

            form_auth_note.html("Введите Ваш Email");
            form_auth_note.addClass('alert-danger');

            $('form#form_auth a.link_submit_auth').css('background', '#ccc');
            $('form#form_auth a.link_submit_auth').css('pointer-events', 'none');
        }
    });


    $("#form_auth a.link_submit_auth").click(function(event){

        event.preventDefault();

        var form_data_serialize = $(this).parents('form').serialize();

        //Проверяем если заполнены все поля
        var required_filds = $(this).parents('form').find("[required]");
        var empty_filds = [];

        var submit_link = $(this);

        $(required_filds).each(function(index){
            if ( $(this).val() == '' ){

                empty_filds[index] = $(this);

                $(this).css('border-bottom-color', '#cc0000');
                $(this).focus();

                form_auth_note.html("Заполните все поля").css({'text-align':'center'});
                
                form_auth_note.addClass('alert-danger');

                submit_link.css('background', '#ccc');
                submit_link.css('pointer-events', 'none');

                setTimeout(function() {

                    form_auth_note.empty();
                    form_auth_note.removeClass('alert-danger');

                    submit_link.css('background', '#ffb400');
                    submit_link.css('pointer-events', 'auto');
                }, 3000);

            }else{
                $(this).css({'border-bottom':'none'});
            }
        });

        if(empty_filds.length == 0){

            //daca emailul e valid
            if(email_validation){

                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     }
                // });

                $.ajax({
                    url: 'auth.php',
                    method: 'POST',
                    dataType: 'json',
                    data: form_data_serialize,

                    // Функция которая будет выполнятся перед отправкой данных
                    beforeSend: function(){
                        form_auth_note.removeClass('alert-danger');
                        form_auth_note.removeClass('alert-success');
                        form_auth_note.addClass('alert-info');
                        form_auth_note.html('Отправляем данные на сервер');
                    },

                    success: function(data) {

                        console.log(data);

                        if (data.result_valid == 'success') {

                            window.location.assign("/");

                        }else{

                            form_auth_note.removeClass('alert-info');
                            form_auth_note.removeClass('alert-success');
                            form_auth_note.addClass('alert-danger');
                            form_auth_note.html(data.message);
                        }

                    },
                    async: false
                });
            }
        }
    });


});