$(document).ready(function () {
    "use strict";

    //================ Проверка email ==================

    //регулярное выражение для проверки email
    var email_validation;
    var pattern_email = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    var formular_register_note_message = $('form#form_register .note_message');
    
    $('form#form_register input[name=email]').blur(function(){
        var email_val = $(this).val();

        if(email_val.length > 0){

            email_validation = pattern_email.test(email_val);

            if(!email_validation){
                

                $(this).css({'border-bottom':'1px solid #cc0000'});

                formular_register_note_message.html("Вы ввели почтовый адрес в неправильном формате.");
                formular_register_note_message.addClass('alert-danger');

                $('form#form_register a#link_submit_register').css('background', '#ccc');
                $('form#form_register a#link_submit_register').css('pointer-events', 'none');

            }else{
                $(this).css({'border-bottom':'none'});

                formular_register_note_message.html('');
                formular_register_note_message.removeClass('alert-danger');

                $.ajax({
                 
                    // Название файла, в котором будем проверять email на существование в базе данных
                    url: "check_email.php", 
                 
                    // Указывываем каким методом будут переданы данные
                    type: "POST", 
                 
                    // Указывываем в формате JSON какие данные нужно передать
                    data: {
                        email: email_val
                    }, 
                     
                    // Тип содержимого которого мы ожидаем получить от сервера.
                    dataType: "html", 
                 
                    // Функция которая будет выполнятся перед отправкой данных
                    beforeSend: function(){
                 
                        formular_register_note_message.text('Проверяем почтовый адерс на занятость');
                    },
                 
                    // Функция которая будет выполнятся после того как все данные будут успешно получены.
                    success: function(data){
                        var data_obj = $.parseJSON(data);

                        if (data_obj.result_valid == 'success') {
                            formular_register_note_message.addClass('alert-success');

                            $('form#form_register a#link_submit_register').css('background', '#ffb400');
                            $('form#form_register a#link_submit_register').css('pointer-events', 'auto');

                        }else{

                            formular_register_note_message.addClass('alert-danger');

                            $('form#form_register a#link_submit_register').css('background', '#ccc');
                            $('form#form_register a#link_submit_register').css('pointer-events', 'none');

                        }

                        formular_register_note_message.empty();
                        //Полученный ответ помещаем внутри тега span
                        formular_register_note_message.text(data_obj.message);
                    } 
                });
            }

        //После очистки ввода, нужно активировать кнопку
        }else{
            email_validation = false;
            $(this).css({'border-bottom':'1px solid #cc0000'});

            formular_register_note_message.html("Введите Ваш Email");
            formular_register_note_message.addClass('alert-danger');

            $('form#form_register a#link_submit_register').css('background', '#ccc');
            $('form#form_register a#link_submit_register').css('pointer-events', 'none');
        }
    });

    //================ Прооверка паролей ==================
    var password = $('form#form_register input[name=password]');
    var confirm_password = $('form#form_register input[name=confirm_password]');

    password.blur(function(){
        if(password.val() != ''){
     

            if(password.val() != confirm_password.val()){

                //Выводим сообщение об ошибке
                formular_register_note_message.html("Пароли не совпадают");
                formular_register_note_message.addClass('alert-danger');

                // Дезактивируем кнопку отправки
                $('form#form_register a#link_submit_register').css('background', '#ccc');
                $('form#form_register a#link_submit_register').css('pointer-events', 'none');
            }else{
                formular_register_note_message.empty();
                formular_register_note_message.removeClass('alert-danger');

                $('form#form_register a#link_submit_register').css('background', '#ffb400');
                $('form#form_register a#link_submit_register').css('pointer-events', 'auto');
            }
            
     
        }else{
            $(this).css({'border-bottom':'1px solid #cc0000'});

            formular_register_note_message.html("Введите пароль.");
            formular_register_note_message.addClass('alert-danger');

            $('form#form_register a#link_submit_register').css('background', '#ccc');
            $('form#form_register a#link_submit_register').css('pointer-events', 'none');
        }
    });

    confirm_password.blur(function(){
        //Если пароли не совпадают
        if(password.val() !== confirm_password.val()){
            
            //Выводим сообщение об ошибке
            formular_register_note_message.html("Пароли не совпадают");
            formular_register_note_message.addClass('alert-danger');

            // Дезактивируем кнопку отправки
            $('form#form_register a#link_submit_register').css('background', '#ccc');
            $('form#form_register a#link_submit_register').css('pointer-events', 'none');

        }else{
            formular_register_note_message.empty();
            formular_register_note_message.removeClass('alert-danger');

            $('form#form_register a#link_submit_register').css('background', '#ffb400');
            $('form#form_register a#link_submit_register').css('pointer-events', 'auto');
        }
     
    });




    $("#link_submit_register").click(function(event){

        event.preventDefault();

        if($("input[name=i_agree]").is(':checked')){

            if(password.val().length < 6){
                //Выводим сообщение об ошибке
                formular_register_note_message.html("Минимальная длина пароля 6 символов");
                formular_register_note_message.addClass('alert-danger');

                // Дезактивируем кнопку отправки
                $('form#form_register a#link_submit_register').css('background', '#ccc');
                $('form#form_register a#link_submit_register').css('pointer-events', 'none');
                
            }else{
                formular_register_note_message.empty();
                formular_register_note_message.removeClass('alert-danger');

                $('form#form_register a#link_submit_register').css('background', '#ffb400');
                $('form#form_register a#link_submit_register').css('pointer-events', 'auto');
            }

            var form_data_serialize = $(this).parents('form').serialize();

            //Проверяем если заполнены все поля
            var required_filds = $(this).parents('form').find("[required]");
            var empty_filds = [];

            var submit_link = $(this);

            $(required_filds).each(function(index){
                if ( $(this).val() == '' ){

                    empty_filds[index] = $(this);

                    $(this).css('border-bottom-color', '#cc0000');
                    $(this).focus();

                    formular_register_note_message.html("Заполните все поля").css({'text-align':'center'});
                    
                    formular_register_note_message.addClass('alert-danger');

                    submit_link.css('background', '#ccc');
                    submit_link.css('pointer-events', 'none');

                    setTimeout(function() {

                        formular_register_note_message.empty();
                        formular_register_note_message.removeClass('alert-danger');

                        submit_link.css('background', '#ffb400');
                        submit_link.css('pointer-events', 'auto');
                    }, 3000);

                }else{
                    $(this).css({'border-bottom':'none'});
                }
            });

            if(empty_filds.length == 0){

                //daca emailul e valid
                if(email_validation){

                    /*$.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
*/
                    $.ajax({
                        url: 'register.php',
                        method: 'POST',
                        dataType: 'json',
                        data: form_data_serialize,

                        // Функция которая будет выполнятся перед отправкой данных
                        beforeSend: function(){
                        
                            formular_register_note_message.text('Отправляем данные на сервер');
                        },

                        success: function(data) {

                            console.log(data);

                            if (data.result_valid == 'success') {

                                formular_register_note_message.html(data.message);
                                formular_register_note_message.addClass('alert-success');

                                // Дезактивируем кнопку отправки
                                $('form#form_register a#link_submit_register').css('background', '#ccc');
                                $('form#form_register a#link_submit_register').css('pointer-events', 'none');

                                /*setTimeout(function() {
                                    formular_register_note_message.empty();
                                    formular_register_note_message.removeClass('alert-success');
                                }, 5000);*/

                            } else {
                                
                                formular_register_note_message.html(data.message);
                                formular_register_note_message.addClass('alert-danger');

                                /*setTimeout(function() {
                                    formular_register_note_message.empty();
                                    formular_register_note_message.removeClass('alert-danger');
                                }, 5000);*/

                            }
                        },
                        async: false
                    });
                }
            }

        }else{

            
                
                //Выводим сообщение об ошибке
                formular_register_note_message.html("Примите условия");
                formular_register_note_message.addClass('alert-danger');

                // Дезактивируем кнопку отправки
                $('form#form_register a#link_submit_register').css('background', '#ccc');
                $('form#form_register a#link_submit_register').css('pointer-events', 'none');

            
            
            setTimeout(function() {
                formular_register_note_message.empty();
                formular_register_note_message.removeClass('alert-danger');

                $('form#form_register a#link_submit_register').css('background', '#ffb400');
                $('form#form_register a#link_submit_register').css('pointer-events', 'auto');
            }, 3000);
            
        }

    });

});