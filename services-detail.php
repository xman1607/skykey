<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Sky Key</title>
<meta name="description" content="" />
<meta name="keywords" content="" />

<!-- Styles -->
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" /><!-- Bootstrap -->
<link rel="stylesheet" href="css/owl.carousel.css" type="text/css" /><!-- Owl Carousal -->	
<link rel="stylesheet" href="css/icons.css" type="text/css" /><!-- Font Awesome -->
<link rel="stylesheet" type="text/css" href="css/jquery.circliful.css" /><!-- Circlful -->
<link rel="stylesheet" href="css/select2.min.css" type="text/css" /><!-- Select2 -->
<link rel="stylesheet" href="css/perfect-scrollbar.css" /><!-- Scroll Bar -->
<link rel="stylesheet" href="css/style.css" type="text/css" /><!-- Style -->	
<link rel="stylesheet" href="css/responsive.css" type="text/css" /><!-- Responsive -->		
<link rel="stylesheet" href="css/colors/color.css" type="text/css" /><!-- Color -->	

</head>
<body itemscope="">
	<div class="theme-layout">
		
        <?php
            require_once("header.php");
        ?>
		
		<div class="page-top blackish overlape">
			<div class="parallax" data-velocity="-.1" style="background: url(https://placehold.it/1600x700) repeat scroll 0 0"></div>
			<div class="container">
				<div class="page-title">
					<span>ИДЕАЛЬНО ДЛЯ ВАС</span>
					<h3>ПОДРОБНО О НАС</h3>
				</div><!-- Page Title -->
			</div>
		</div><!-- Page Top -->


		<section class="block">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="">
							<div class="row">
								<div class="col-md-4">
									<div class="services-menu">
										<div class="heading2">
											<span>Идеально для Вас</span>
											<h2>СПИСОК УСЛУГ</h2>
										</div>
										<ul>
											<li><a href="services-detail.html" title="" itemprop="url">Упаковка и хранение</a></li>
											<li><a href="services-detail2.html" title="" itemprop="url">Транспортировка</a></li>
											<li><a href="services-detail3.html" title="" itemprop="url">Хранение</a></li>
											<li><a href="services-detail4.html" title="" itemprop="url">Сопровождение</a></li>
											<li><a href="services-detail5.html" title="" itemprop="url">Доставка до двери</a></li>
										</ul>
									</div>
								</div>
								<div class="col-md-8">
									<div class="services-details">
										<div class="service-detail-post">
											<div class="services-thumb">
												<img src="../../themeforest-14696898-unload-cargo-shipping-warehouse-transport-html5-responsive-website-template_2/HTML FILES/images/2.jpg" alt="" itemprop="url" />
											</div>
											<div class="services-info">
												<h2 itemprop="name">Упаковка и хранение</h2>
												<span>Условия поставки</span>
											</div>
										</div>
									</div><!-- Services Details -->
									<div id="services-detail-tabs">
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">ОПИСАНИЕ</a></li>
											<li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">КОМЕНТАРИИ</a></li>
											<li role="presentation"><a href="#chart" aria-controls="chart" role="tab" data-toggle="tab">ГРАФИК</a></li>
										</ul>
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane fade in active" id="description">
												<div class="services-tabs-content">
													<p itemprop="description">Воспользовавшись услугами компании Sky Key, Вы получаете возможность покупать товары в интернет магазинах и на интернет аукционах Америки и Канады, даже если продавец не осуществляет доставку в Ваш город.</p>
													<p itemprop="description">Зарегистрировавшись на нашем сайте Вы получаете индивидуальный личный кабинет и личный адрес в Америке и Канаде, которые Вы можете использовать при покупках. Совершив покупку в Америке, Вы в личном кабинете указываете название интернет магазина, описание содержимого посылки, стоимость каждого товара и примерный вес. В момент доставки, вам на адрес электронной почты, который вы указали при регистрации, придет уведомление о том что она находится на нашем специально оборудованном складе. В дальнейшем Вы можете объединить несколько товаров из разных магазинов в одну большую посылку, выбрать наиболее подходящий Вам метод доставки, и отправить сборную посылку к себе. При желании Вы конечно можете не объединять посылки, а отправить любую отдельно взятую без переупаковки, или например разделить одну поступившую на наш склад посылку на несколько более мелких.</p>
                                                    
   <p itemprop="description">Мы предлагаем Вам воспользоваться нашим калькулятором стоимости доставки чтобы получить примерную стоимость доставки посылки, в зависимости от веса, пункта назначения, и способа доставки.</p>


<h2>Доставка посылок из Америки</h2>                                    
<p itemprop="description"> Компания Sky Key доставляет товары из Америки и Канады в 150 стран мира, включая страны СНГ.
Мы предлагаем Вам использовать при покупках в интернет-магазинах и на интернет-аукционах (включая Amazon и Ebay) в качестве Вашего адреса доставки любой из наших складов, в том числе в безналоговом штате Делавер (что позволит вам сэкономить на sales taxes – налогах на продажу) и Канады. </p>

<p itemprop="description"> Мы рады Вам предложить самую низкую стоимость доставки из Америки за счет того, что компания имеет собственную логистику и контрактные цены со многими службами доставки.
На нашем сайте, воспользовавшись удобным виртуальным калькулятором, Вы можете самостоятельно подсчитать сколько будет стоить доставка из Америки, однако мы обращаем Ваше внимание на то, что Вы увидите предварительную стоимость доставки. Окончательную стоимость доставки можно будет определить после того, как посылка будет сформирована на складе. </p>

<p itemprop="description"> Компания Sky Key доставляет посылки своей логистикой в Россию и Узбекистан. При доставке методом ПЭ-Стандарт маршрут исключает остановки в транзите. Однако для оформления доставки ПЭ-Стандарт необходимо соблюдать правила оформления документов, с подробным описанием этих правил Вы можете ознакомиться на странице.
По желанию клиента возможна доставка почтовой службой USPS (EMS, Priority, Airmail) или международными курьерскими компаниями. </p>
        
        
  <p itemprop="description"> Вы сами выбираете как Ваши покупки будут Вам доставлены. Возможна почтовая и курьерская доставка. При почтовой доставке Вы самостоятельно забираете свою посылку в Вашем местном почтовом отделении, при курьерской доставке Вашу посылку к Вам на дом привезёт курьер. </p>  
  
  
  <p itemprop="description"> Мы самостоятельно и совершенно бесплатно оформляем таможенные и иные документы которые должны быть у посылки, согласно международным нормативам тех стран, куда она будет отправлена. </p>
  
  
  
  <p itemprop="description"> При отправке все посылки страхуются. Страхование посылки на $100.00 уже включено в стоимость доставки.
Мы бесплатно предоставляем сервис отслеживания местонахождения посылки. </p>                                                     
												</div>
											</div>
											<div role="tabpanel" class="tab-pane fade" id="reviews">
												<div class="services-tabs-content comment-main">
													<ul itemscope="" itemtype="https://schema.org/UserComments">
														<li>
															<div class="comment">
																<img itemprop="image" alt="" src="https://placehold.it/170x186">
																<div class="comment-detail">
																	<div class="comment-info">
																		<h6 itemprop="creator" itemscope="" itemtype="https://schema.org/Person">
																			<a itemprop="url" href="" title="">Margaret</a>
																			<span>says:</span>
																		</h6>
																		<i itemprop="commentTime">November 17, 2014 at 9:55 am </i>
																		<a itemprop="url" href="#" title="" class="theme-btn">REPLY</a>
																	</div>
																	<p itemprop="commentText">Curabitur blandit tempus porttitor. Maecenas sediam eget rius blandit site accumsan urna molestie interdum.</p>
																</div>
															</div>
															<ul itemscope="" itemtype="https://schema.org/UserComments">
																<li>
																	<div class="comment reply">
																		<img itemprop="image" alt="" src="https://placehold.it/170x186">
																		<div class="comment-detail">
																			<div class="comment-info">
																				<h6 itemprop="creator" itemscope="" itemtype="https://schema.org/Person">
																					<a itemprop="url" href="" title="">Garet</a>
																					<span>says:</span>
																				</h6>
																				<i itemprop="commentTime">November 17, 2014 at 9:55 am </i>
																				<a itemprop="url" href="#" title="" class="theme-btn">REPLY</a>
																			</div>
																			<p itemprop="commentText">Curabitur blandit tempus porttitor. Maecenas sediam eget rius blandit site accumsan urna molestie interdum.</p>
																		</div>
																	</div>
																</li>
															</ul>
														</li>
													</ul>
												</div>
                                                <div class="leave-reply">
                                                    <div class="heading6">
                                                        <h3>Leave A Reply</h3>
                                                        <p>Your email address will not be published. Required fields are marked *</p>
                                                    </div>
                                                    <div class="reply-form">
                                                        <form>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="text" placeholder="Full Name *" class="text-field" />
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <input type="email" placeholder="Email Address *" class="text-field" />
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <input type="text" placeholder="Subject *" class="text-field" />
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <textarea placeholder="Message *" class="text-field"></textarea>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <a class="theme-btn" href="#" title="" itemprop="url"><i class="fa fa-paper-plane"></i>POST COMMENT</a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div><!-- leave Replay -->
											</div>
											<div role="tabpanel" class="tab-pane fade" id="chart">
												<div class="services-tabs-content">
													<p itemprop="description">Aagnis dis parturient monte, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit ametum penatibus. Nunc rhoncus rutrum leo id venenatis.</p>
													<div class="chart-detail">
														<div class="row">
															<div class="col-md-6">
																<div class="chart-rating">
																	<div id="rating-chart" data-startdegree="60" data-dimension="250" data-text="78%" data-info="Satisfide Users" data-width="7" data-fontsize="30" data-percent="78" data-fgcolor="#f5b120" data-bgcolor="#ededed"></div>
																</div>
															</div>
															<div class="col-md-6">
																<div class="address-book">
																	<ul>
																		<li><span>Time:</span> Within 5 Days</li>
																		<li><span>Email Id:</span> user@gmail.com</li>
																		<li><span>Office Address:</span> 225 Main Street London England</li>
																	</ul>
																	<a class="theme-btn" href="order-now.html" itemprop="url" title=""><i class="fa fa-paper-plane"></i>Order Now</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div><!-- Services Detail Tabs -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		

		<footer>
			<section class="block">
				<div class="parallax dark" data-velocity="-.2" style="background: rgba(0, 0, 0, 0) url(https://placehold.it/1600x700) no-repeat 50% 0;"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3 column">
									<div class="widget">
										<div class="about-widget">
											<div class="logo">
												<a itemprop="url" href="index.html" title=""><img itemprop="image" src="images/resource/logo.png" alt="" /></a>
											</div>
											<div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Адрес доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Описание сервиса</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Таможенные лимиты</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Ограничения доставки</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Блог компании</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Расчет мощности LI-Ion АКБ</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            <ul class="social-btn">
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#" title="" itemprop="url"><i class="fa fa-facebook"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 column">
                                    <Div class="row">
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>ССЫЛКИ НА....</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="about.html" title="">Условия доставки</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Получение посылок</a></li>
                                                        <li><a itemprop="url" href="events.html" title="">Отслеживание посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Доставка негабарита</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Доставка с Амазона</a></li>
                                                        <li><a itemprop="url" href="blog-grid-3column.html" title="">Ребейты</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                        <div class="col-md-6 column">
                                            <div class="widget">
                                                <div class="heading2">
                                                    <span>Быстро и надежно</span>
                                                    <h3>Услуги доставки</h3>
                                                </div>
                                                <div class="links-widget">
                                                    <ul>
                                                        <li><a itemprop="url" href="services-detail2.html" title="">ПЭ Стандарт</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail6.html" title="">Доставка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail3.html" title="">Отправка посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail4.html" title="">Страхование посылок</a></li>
                                                        <li><a itemprop="url" href="services-detail.html" title="">Доставка с Ебей</a></li>
                                                        
                                                        <li><a itemprop="url" href="services-detail.html" title="">Форум</a></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div><!-- Widget -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 column">
                                    <div class="widget blue1">
                                        <div class="heading2">
                                            <span>БЫСТРО И НАДЕЖНО</span>
                                            <h3>ОТПРАВИТЬ ПИСЬМО</h3>
                                        </div>
                                        <div class="subscription-form">
                                            <p itemprop="description">ЦИТАТА ИЛИ ЕЩЕ ЧТО ТО</p>
                                            <form>
                                                <input type="text" placeholder="Enter Your Email Address" />
                                                <a title="" href="#" class="theme-btn" data-toggle="modal" data-target="#submission-message"><i class="fa fa-paper-plane"></i>ОТПРАВИТЬ</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bottom-line">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 column">
                            <span>&copy; 2015 <a itemprop="url" title="" href="index.html">Unload</a> - All Rights Reserved - Made By Webinane</span>
                        </div>
                        <div class="col-md-6 column">
                            <ul>
                                <li><a itemprop="url" href="index.html" title="">HOME</a></li>
                                <li><a itemprop="url" href="services.html" title="">SERVICES</a></li>
                                <li><a itemprop="url" href="packages.html" title="">OUR RATES</a></li>
                                <li><a itemprop="url" href="contact.html" title="">CONTACT</a></li>
                                <li><a itemprop="url" href="about.html" title="">ABOUT US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blank"></div>
        </footer>		

    </div>

    <!-- Region Popup -->
    <div class="modal fade region" id="region" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="images/close.png" alt="" itemprop="image" /></span></button>
                    <div class="region-detail">
                        <div class="row">
                            <div class="col-md-6 column">
                                <div class="region-contact-info">
                                    <div class="heading2">
                                        <span>Fast And Safe</span>
                                        <h3>Office Address</h3>
                                    </div>
                                    <p>Transport logitec, ltd. 2258 millenioum Street Columbia, DK 85966</p>
                                    <div class="contact-detail">
                                        <span class="contact">
                                            <i class="fa fa-mobile"></i>
                                            <strong>Phone No</strong>
                                            <span>+858 5549 512</span>
                                            <span>+858 5549 512</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-email"></i>
                                            <strong>Email Address</strong>
                                            <span>info@transport@gmail.com</span>
                                        </span>
                                        <span class="contact">
                                            <i class="fa fa-time"></i>
                                            <strong>Office Timing</strong>
                                            <span>10:00am - 06:00pm / Sunday: Close</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 column">
                                <div class="loc-thumb">
                                    <img src="https://placehold.it/340x222" alt="" itemprop="image" />
                                    <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit. Aennean commodo enean dolor sit amet, consectetuer.</p>
                                    <a class="theme-btn" href="#" title="" itemprop="url">СВЯЖИТЕСЬ С НАМИ СЕЙЧАС</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- Region Detail -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="submission-message" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="submission-data">
                        <span><img src="images/resource/submission.png" alt="" /></span>
                        <h1>СВЯЖИТЕСЬ С НАМИ</h1>
                        <p>СЛОГАН ИЛИ БЛАГОДАРНОСТЬ</p>
                        <a href="#" title="" class="theme-btn" data-dismiss="modal" aria-label="Close"><i class="fa fa-paper-plane"></i>ВЕРНУТЬСЯ</a>
                    </div><!-- Submission-data -->
                </div>
            </div>
        </div>
    </div>
	
<!-- Script -->
<script type="text/javascript" src="js/modernizr-2.0.6.js"></script><!-- Modernizr -->
<script type="text/javascript" src="js/jquery-2.2.2.js"></script><!-- jQuery -->
<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Bootstrap -->
<script type="text/javascript" src="js/scrolltopcontrol.js"></script><!-- Scroll To Top -->
<script type="text/javascript" src="js/jquery.scrolly.js"></script><!-- Scrolly -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Owl Carousal -->
<script type="text/javascript" src="js/icheck.js"></script><!-- iCheck -->
<script type="text/javascript" src="js/select2.full.js"></script><!-- Select2 -->
<script type="text/javascript" src="js/jquery.circliful.min.js"></script><!-- Circliful -->
<script type="text/javascript" src="js/perfect-scrollbar.js"></script><!-- Scroll Bar -->
<script type="text/javascript" src="js/perfect-scrollbar.jquery.js"></script><!-- Scroll Bar -->
<script src="js/script.js"></script>
<script src="js/valid_form_register.js"></script>
<script src="js/valid_form_auth.js"></script>

<script>
$(document).ready(function() {
	"use strict";

	//** Rating Chart **//
	$('#rating-chart').circliful();		

});
</script>
</body>
</html>